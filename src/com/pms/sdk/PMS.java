package com.pms.sdk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DataKeyUtil;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.FCMRequestToken;

import java.io.Serializable;

/**
 * @since 2012.12.28
 * @author erzisk
 * @description pms (solution main class)
 * @version [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 *          [2015.02.04 10:23] SDK 사용 중지 요청 가능하게 수정함. <br>
 *          [2015.02.05 19:33] newMsg Update시 DEL_YN값이 null값이 들어가는 현상 수정함. <br>
 *          [2015.02.11 09:55] Flag 값들이 안바뀌는 현상 수정함. <br>
 *          [2015.02.25 11:45] 앱 최초설치시에는 msgFlag & notiFlag 값 서버에서 받아온 값 저장함. <br>
 *          [2015.03.03 16:55] 앱실행시 체크하는 프로세스 롤리팝 버전에 맞게 수정함.<br>
 *          [2015.03.11 10:02] Private 서버 안정화.<br>
 *          [2015.03.17 18:11] 팝업창 노출 플래그 추가함.<br>
 *          [2015.03.19 16:29] DB쪽 버그 수정함.<br>
 *          [2015.03.30 21:34] 팝업 클릭시 ClickMsg & readMsg 직접호출로 수정함.<br>
 *          [2015.04.03 09:40] Notification Image Load가 실패하면 Text Notification으로 전환하는 코스 삽입함.<br>
 *          [2015.04.23 16:59] GCM 받아오는 부분 팝업창 뜨도록 되어 있는부분을 로그로 찍게 수정함. <br>
 *          [2015.06.03 10:12] 푸쉬 페리로드값에 따른 팝업창 재어 수정함. <br>
 *          [2015.10.21 14:07] Android 6.0 대비코드 수정함. NewMsgCnt값 MsgGrpCd로 받을 수 있게 수정함. <br>
 *          [2016.04.29 10:21] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 *          [2016.10.24 10:56] 정통망법 적용함.<br>
 *          [2016.12.02 18:36] doze mode broadcast 패치1 적용 <br>
 *          [2017.08.11 14:09] 저장권한 제거 <br>
 *          [2017.10.30 15:40] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 *          					* DB 중복 Open으로 인해 강제종료 문제 수정
 *          					* PushPopup 발생 시 강제 종료 문제 수정
 *          					* UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 *          					  UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함 <br>
 *          [2018.09.27 14:05] 방해금지 모드 추가, Android 8.0 대응, WRITE_EXTERNAL_PERMISSION 제거, 벨소리 볼륨, MQTT WakeupTime 적용
 *          [2018.12.03 15:18] MQTTStarter 적용, Ringtone 대응
 *          [2018.12.18 18:16] MQTTBinder 적용, 벨소리 대응
 *          [2019.01.04 15:58] MQTT 동작안하도록 변경, GcmIntentService 적용
 *			[2019.03.13 10:23] FCM 적용, 벨소리 볼륨 핸들러 적용
 *			[2019.04.15 09:33] FCM 토큰 발급 개선, 벨소리 볼륨 조절 로직 제거
 *			[2019.04.22 11:09] FCM 토큰 발급 개선 로직 중 조건이 반대로 있던거 수정
 *			[2019.07.05 11:17] 웹뷰 크로스앱스크립팅 이슈로 인한 기본팝업 삭제
 *			[2019.11.15 17:46] Android 10 IMEI 값 못가져오는 이슈 대응하기 위해 DeviceCert에 AndroidId 파라미터 추가함, 앱내 메시지 사용 개선
 *			[2019.11.20 09:51] 앱내메시지 호출 메소드 가이드에 없는거 있다고 해서 추가해줌
 *			[2019.11.26 15:12] 앱내메시지 호출 버그 있어서 수정함
 *			[2020.08.31 09:23] 업체 요청으로 AndroidX 미대응으로 롤백(다른 라이브러리에서 문제생겨서 보류했다고함)
 *			[2020.11.10 14:55] 업체 요청으로 AndroidX 대응(내부 이슈는 담당자가 대처하겠다고함)
 */
public class PMS implements IPMSConsts, Serializable {

	private static final long serialVersionUID = 1L;

	private static PMS instancePms = null;

	private static PMSPopup instancePmsPopup = null;

	private Context mContext = null;

	private PMSDB mDB = null;

	private PMS(Context context) {
		this.mDB = PMSDB.getInstance(context);
		CLog.i("Version:" + PMS_VERSION + ",UpdateDate:202011101455");

		initOption(context);
	}

	public static PMS getInstance (final Context context) {
		CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
		CLog.setDebugMode(context);
		CLog.setDebugName(context);
		if (instancePms == null) {
			instancePms = new PMS(context);
		}
		instancePms.setmContext(context);

		String token = PMSUtil.getGCMToken(context);
		if (TextUtils.isEmpty(token) || NO_TOKEN.equals(token))
		{
			new FCMRequestToken(context, PMSUtil.getGCMProjectId(context), new FCMRequestToken.Callback() {
				@Override
				public void callback(boolean isSuccess, String message)
				{
					CLog.d("FCMRequestToken " +isSuccess+" / "+message);

				}
			}).execute();
		}
		return instancePms;
	}

	public static PMSPopup getPopUpInstance () {
		return instancePmsPopup;
	}

	public static boolean clear () {
		try {
			PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
			instancePms = null;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void initOption (Context context) {
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_RING_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_RING_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_VIBE_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_VIBE_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_ALERT_FLAG))) {
			DataKeyUtil.setDBKey(context, DB_ALERT_FLAG, "Y");
		}
		if (StringUtil.isEmpty(DataKeyUtil.getDBKey(context, DB_MAX_USER_MSG_ID))) {
			DataKeyUtil.setDBKey(context, DB_MAX_USER_MSG_ID, "-1");
		}
	}

	public void setCustId (String custId) {
		PMSUtil.setCustId(mContext, custId);
	}

	public String getCustId () {
		return PMSUtil.getCustId(mContext);
	}

	private void setmContext (Context context) {
		this.mContext = context;
	}

//	public void setPopupSetting (Boolean state, String title) {
//		instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
//	}

	public void setIsPopupActivity (Boolean ispopup) {
		PMSUtil.setPopupActivity(mContext, ispopup);
	}

	public void setNotiOrPopup (Boolean isnotiorpopup) {
		PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
	}

	public void setRingMode (boolean isRingMode) {
		DataKeyUtil.setDBKey(mContext, DB_RING_FLAG, isRingMode ? "Y" : "N");
	}

	public void setVibeMode (boolean isVibeMode) {
		DataKeyUtil.setDBKey(mContext, DB_VIBE_FLAG, isVibeMode ? "Y" : "N");
	}

	public void setPopupNoti (boolean isShowPopup) {
		DataKeyUtil.setDBKey(mContext, DB_ALERT_FLAG, isShowPopup ? "Y" : "N");
	}

	public String getMsgFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MSG_FLAG);
	}

	public String getNotiFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_NOTI_FLAG);
	}

	public String getMktFlag () {
		return DataKeyUtil.getDBKey(mContext, DB_MKT_FLAG);
	}

	public String getMaxUserMsgId ()
	{
		return mDB.selectLastUserMsgId();
	}

	/*
	 * ===================================================== [start] database =====================================================
	 */
	/**
	 * select MsgGrp list
	 * 
	 * @return
	 */
	public Cursor selectMsgGrpList () {
		return mDB.selectMsgGrpList();
	}

	/**
	 * select MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public MsgGrp selectMsGrp (String msgCode) {
		return mDB.selectMsgGrp(msgCode);
	}

	/**
	 * select new msg Cnt
	 * 
	 * @return
	 */
	public int selectNewMsgCnt () {
		return mDB.selectNewMsgCnt();
	}

	public int selectNewMsgCnt (String msgGrpCd) {
		return mDB.selectNewMsgCnt(msgGrpCd);
	}

	/**
	 * select Msg List
	 * 
	 * @param page
	 * @param row
	 * @return
	 */
	public Cursor selectMsgList (int page, int row) {
		return mDB.selectMsgList(page, row);
	}

	/**
	 * select Msg List
	 * 
	 * @param msgCode
	 * @return
	 */
	public Cursor selectMsgList (String msgCode) {
		return mDB.selectMsgList(msgCode);
	}

	/**
	 * select Msg
	 * 
	 * @param msgId
	 * @return
	 */
	public Msg selectMsgWhereMsgId (String msgId) {
		return mDB.selectMsgWhereMsgId(msgId);
	}

	/**
	 * select Msg
	 * 
	 * @param userMsgID
	 * @return
	 */
	public Msg selectMsgWhereUserMsgId (String userMsgID) {
		return mDB.selectMsgWhereUserMsgId(userMsgID);
	}

	/**
	 * update MsgGrp
	 * 
	 * @param msgCode
	 * @param values
	 * @return
	 */
	public long updateMsgGrp (String msgCode, ContentValues values) {
		return mDB.updateMsgGrp(msgCode, values);
	}

	/**
	 * update read msg
	 * 
	 * @param msgGrpCd
	 * @param firstUserMsgId
	 * @param lastUserMsgId
	 * @return
	 */
	public long updateReadMsg (String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
		return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
	}

	/**
	 * update read msg where userMsgId
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long updateReadMsgWhereUserMsgId (String userMsgId) {
		return mDB.updateReadMsgWhereUserMsgId(userMsgId);
	}

	/**
	 * update read msg where msgId
	 * 
	 * @param msgId
	 * @return
	 */
	public long updateReadMsgWhereMsgId (String msgId) {
		return mDB.updateReadMsgWhereMsgId(msgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param userMsgId
	 * @return
	 */
	public long deleteUserMsgId (String userMsgId) {
		return mDB.deleteUserMsgId(userMsgId);
	}

	/**
	 * delete Msg
	 * 
	 * @param MsgId
	 * @return
	 */
	public long deleteMsgId (String MsgId) {
		return mDB.deleteMsgId(MsgId);
	}

	/**
	 * delete MsgGrp
	 * 
	 * @param msgCode
	 * @return
	 */
	public long deleteMsgGrp (String msgCode) {
		return mDB.deleteMsgGrp(msgCode);
	}

	/**
	 * delete expire Msg
	 * 
	 * @return
	 */
	public long deleteExpireMsg () {
		return mDB.deleteExpireMsg();
	}

	/**
	 * delete empty MsgGrp
	 * 
	 * @return
	 */
	public void deleteEmptyMsgGrp () {
		mDB.deleteEmptyMsgGrp();
	}

	/**
	 * delete all
	 */
	public void deleteAll () {
		mDB.deleteAll();
	}

	/*
	 * ===================================================== [end] database =====================================================
	 */
	public void setDoNotDisturb(Context context, boolean isEnable)
	{
		DataKeyUtil.setDBKey(context, DB_DO_NOT_DISTURB_FLAG, isEnable?FLAG_Y:FLAG_N);
	}
	public void setDoNotDisturbTime(Context context, String fromTimeFormatLikeHHMM, String toTimeFormatLikeHHMM)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(fromTimeFormatLikeHHMM);
		builder.append(toTimeFormatLikeHHMM);
		DataKeyUtil.setDBKey(context, DB_DO_NOT_DISTURB_TIME, builder.toString());
	}

	public Cursor selectAllOfMsg () {
		return mDB.selectAllOfMsg();
	}
	public Cursor selectAllOfMsgByMsgGrpCode (String msgGrpCode) {
		return mDB.selectAllOfMsgByMsgGrpCode(msgGrpCode);
	}
	public Cursor selectAllOfMsgGrp () {
		return mDB.selectAllOfMsgGrp();
	}
	public int getAllOfUnreadCount() {return mDB.getAllOfUnreadMsgCount();}

	public int getUnreadCountByMsgGrpCode(String msgGrpCode) { return mDB.getUnreadMsgCountByMsgGrpCode(msgGrpCode);}
	public Cursor selectMsgList (String msgGrpCode, int page, int row)
	{
		return mDB.selectMsgList(msgGrpCode, page, row);
	}
}
