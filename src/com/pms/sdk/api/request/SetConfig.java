package com.pms.sdk.api.request;

import org.json.JSONObject;

import android.content.Context;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.DataKeyUtil;

public class SetConfig extends BaseRequest {

	public SetConfig(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @param msgFlag
	 * @param notiFlag
	 * @return
	 */
	public JSONObject getParam (String msgFlag, String notiFlag, String mktFlag) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("msgFlag", msgFlag);
			jobj.put("notiFlag", notiFlag);
			jobj.put("mktFlag", mktFlag);
			jobj.put("mktVer", FLAG_Y);
			jobj.put("loginState", DataKeyUtil.getDBKey(mContext, DB_LOGINED_STATE));
			return jobj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param apiCallback
	 */
	public void request (String msgFlag, String notiFlag, String mktFlag, final APICallback apiCallback) {
		try {
			apiManager.call(API_SET_CONFIG, getParam(msgFlag, notiFlag, mktFlag), new APICallback() {
				@Override
				public void response (String code, JSONObject json) {
					if (CODE_SUCCESS.equals(code)) {
						requiredResultProc(json);
					}
					if (apiCallback != null) {
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json) {
		try {
			DataKeyUtil.setDBKey(mContext, DB_MSG_FLAG, json.getString("msgFlag"));
			DataKeyUtil.setDBKey(mContext, DB_NOTI_FLAG, json.getString("notiFlag"));
			DataKeyUtil.setDBKey(mContext, DB_MKT_FLAG, json.getString("mktFlag"));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
