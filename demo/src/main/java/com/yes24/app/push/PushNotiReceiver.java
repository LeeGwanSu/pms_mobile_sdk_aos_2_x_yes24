package com.yes24.app.push;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.pms.sdk.api.request.ReadMsg;
import com.pms.sdk.bean.PushMsg;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.yes24.app.activity.MainActivity;

/**
 * Notibar Click 시 실행되는 class
 *
 */
public class PushNotiReceiver extends BroadcastReceiver {

	@SuppressLint("ShowToast")
	@Override
	public void onReceive (Context context, Intent intent) {
		CLog.i("onReceive");
		Intent i = new Intent(context, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		i.putExtras(intent.getExtras());

		// 버튼 링크값 가져오는 방법.
		try {
			PushMsg pushMsg = new PushMsg(i.getExtras());
			JSONObject btnLink = new JSONObject(pushMsg.data);
			Toast.makeText(context, btnLink.get("l").toString(), Toast.LENGTH_SHORT).show();

			JSONArray reads = new JSONArray();
			reads.put(PMSUtil.getReadParam(pushMsg.msgId));

			new ReadMsg(context).request(reads, null);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		context.startActivity(i);
	}
}